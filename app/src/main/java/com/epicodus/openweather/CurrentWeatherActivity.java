package com.epicodus.openweather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CurrentWeatherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_weather);
    }
}
