package com.epicodus.openweather;

/**
 * Created by Guest on 7/5/16.
 */
public class Constants {
    public static final API_KEY = BuildConfig.API_KEY;
    public static final API_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast?q=";
    public static final YOUR_QUERY_PARAMETER = "location"; //Example: "location"
    public static final API_KEY_QUERY_PARAMETER = "appid";
}


